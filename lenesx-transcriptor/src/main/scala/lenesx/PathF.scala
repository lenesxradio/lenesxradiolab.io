package lenesx

import scala.Unit
import scala.Predef.String
import cats.effect.*
import cats.effect.syntax.all.*
import fs2.*
import cats.MonadThrow
import fs2.io.file.Path

object PathF {

  def apply[F[_]](path: String)(using F: MonadThrow[F]): F[Path] = F.catchNonFatal(Path.apply(path))

  def jvmResource[F[_]](path: String)(using F: MonadThrow[F]): F[Path] = F.catchNonFatal(
    Path.fromNioPath(java.nio.file.Path.of(java.lang.ClassLoader.getSystemResource(path).toURI))
  )

}
