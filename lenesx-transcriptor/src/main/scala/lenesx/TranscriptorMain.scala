package lenesx

import scala.Unit
import scala.Predef.String
import cats.syntax.all.*
import cats.effect.*
import cats.effect.syntax.all.*
import fs2.*

import cats.effect.std.Console
import fs2.io.file.*

object TranscriptorMain extends IOApp.Simple:
  override def run: IO[Unit] =
    given Files[IO]            = Files.forAsync[IO]
    given console: Console[IO] = Console.make[IO]
    runF[IO]
  end run

  def runF[F[_]](using F: Temporal[F], files: Files[F], console: Console[F]): F[Unit] =
    for {
      readFrom <- PathF.jvmResource[F]("transcript.txt").map(_.absolute)
      writeTo  <- PathF[F]("result.html").map(_.absolute)
      _        <- console.println(s"reading from: $readFrom")
      _        <- console.println(s"writing to:   $writeTo")
      duration <- readThenWriteStream[F](readFrom = readFrom, writeTo = writeTo).compile.drain.timed.map(_._1)
      _        <- console.println(s"completed after ~${duration.toMillis}ms")
    } yield ()

  def readThenWriteStream[F[_]](readFrom: Path, writeTo: Path)(using files: Files[F]): Stream[F, Unit] =
    (Stream.emit("""<div class="section_transcript">""") ++
      Stream.emit("""<h3>Transcript</h3>""") ++
      files
        .readUtf8Lines(readFrom)
        .map(_.trim)
        .filterNot(_.isBlank)
        .map(p => s"<p>$p</p>") ++
      Stream.emit("""</div>"""))
      .map(_.concat("\n"))
      .through(fs2.text.utf8.encode[F])
      .through(files.writeAll(writeTo))

end TranscriptorMain
