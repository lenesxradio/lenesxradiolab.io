val scala3V = "3.4.2"  // https://github.com/lampepfl/dotty/releases
val fs2V    = "3.11.0" // https://github.com/typelevel/fs2/releases

val `lenesx-transcriptor` = project
  .in(file("./"))
  .settings(
    name                            := "lenesx-transcriptor",
    scalaVersion                    := scala3V,
    libraryDependencies += "co.fs2" %% "fs2-io" % fs2V,
    scalacOptions ++= compilerFlags
  )

def compilerFlags: Seq[String] = Seq(
  "-Yno-imports",
  "source:future",
  "new-syntax"
)
