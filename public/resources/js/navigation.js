$( document ).ready(function(){    
    $( "#goto_all" ).on( "click", function() {
		$( "li" ).removeClass( "lan_current" );
		$( "#goto_all" ).addClass( "lan_current" );

		$( ".entry" ).css( 'display', 'block' );
		getMotto();
		getRandom();

		$( "#contact_lan").attr("href", "https://lenesxradio.ro/pages/contact-EN.html" );

	}); 

    $( "#goto_en" ).on( "click", function() {
		$( "li" ).removeClass( "lan_current" );
		$( "#goto_en" ).addClass( "lan_current" );

		$( ".entry" ).css( 'display', 'none' );
		$( ".par_en" ).css( 'display', 'block' );
		getMotto();
		getRandom();

		$( "#contact_lan").attr("href", "https://lenesxradio.ro/pages/contact-EN.html" );
	});

    $( "#goto_ro" ).on( "click", function() {
		$( "li" ).removeClass( "lan_current" );
		$( "#goto_ro" ).addClass( "lan_current" );

		$( ".entry" ).css( 'display', 'none' );
		$( ".par_ro" ).css( 'display', 'block' );
		getMotto();
		getRandom();

		$( "#contact_lan").attr("href", "https://lenesxradio.ro/pages/contact-RO.html" );
	});

    $( "#goto_de" ).on( "click", function() {
		$( "li" ).removeClass( "lan_current" );
		$( "#goto_de" ).addClass( "lan_current" );

		$( ".entry" ).hide();
		$( ".par_de" ).show();
		getMotto();
		getRandom();
	});

    $( "#goto_hu" ).on( "click", function() {
		$( "li" ).removeClass( "lan_current" );
		$( "#goto_hu" ).addClass( "lan_current" );

		$( ".entry" ).hide();
		$( ".par_hu" ).show();
		getMotto();
		getRandom();
	});             

});
