function getMotto()
{
    let $motto_no = Math.floor( Math.random() * 2 ); 
    let $lan_current = $(".lan_current").text();
    
    var $motto_list = [
        [ "Radical sloth, intersectional sloth, anti-authoritarian sloth",
          "The leftist podcast you can listen to when you don’t feel like doing anything" ],    // all
        [ "Radical sloth, intersectional sloth, anti-authoritarian sloth",
          "The leftist podcast you can listen to when you don’t feel like doing anything" ],    // en
        [ "Lene radicală, lene intersecțională, lene antiautoritară",
          "Podcastul de stânga pe care îl poți asculta când nu ai chef de nimic" ],             // ro
        [ "WIP", "WIP" ],                                                                        // de
        [ "WIP", "WIP" ]                                                                         // hu
    ];

    var $lan_no = 0;
    if ( $lan_current == "All" ) {
        $lan_no = 0;
    } else if ( $lan_current == "EN" )  {
        $lan_no = 1;
    } else if ( $lan_current == "RO" )  {
        $lan_no = 2;
    } else if ( $lan_current == "DE" )  {
        $lan_no = 3;
    } else if ( $lan_current == "HU" )  {
        $lan_no = 4;
    }

    $(".logo_motto").html( $motto_list[$lan_no][$motto_no] );
}

$( document ).ready(function(){    
    getMotto();
    getRandom();
});
