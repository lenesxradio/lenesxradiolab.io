function getRandomEp()
{
    let $rand_no = 32;          // episodes so far
    let $lan_no = 0;
    let $lan_current = $(".lan_current").text();

    if ( $lan_current == "EN" )  {
        $rand_no = 10;
        $lan_no = 1;
    } else if ( $lan_current == "RO" )  {
        $rand_no = 22;
        $lan_no = 2;
    } else if ( $lan_current == "DE" )  {
        $rand_no = 0;
    } else if ( $lan_current == "HU" )  {
        $rand_no = 0;
    }

    var $ep_list = [        // all en ro
        [ '/-01/lenesx--01-covid-EN.html', '/-01/lenesx--01-covid-EN.html', '/-01/lenesx--01-covid-RO.html' ],
        [ '/-01/lenesx--01-covid-RO.html', '/002/lenesx-002-anti-eviction-EN.html', '/001/lenesx-001-aldessa-RO.html'],
        [ '/001/lenesx-001-aldessa-RO.html', '/012/lenesx-012-marina-EN.html', '/003/lenesx-003-alex-adi-RO.html' ],
        [ '/002/lenesx-002-anti-eviction-EN.html', '/014/lenesx-014-EAST-EN.html', '/004/lenesx-004-mad-pride-RO.html' ],
        [ '/003/lenesx-003-alex-adi-RO.html', '/016/lenesx-016-elmar-sergiu-migrant-EN.html', '/005/lenesx-005-laci-RO.htm' ],
        [ '/004/lenesx-004-mad-pride-RO.html', '/017/lenesx-017-rasha-free-palestine-EN.html', '/006/lenesx-006-antonella-RO.html' ],
        [ '/005/lenesx-005-laci-RO.html', '/023/lenesx-023-effrosyni-workers-self-management-viome-EN.html', '/007/lenesx-007-nico-RO.html' ],
        [ '/006/lenesx-006-antonella-RO.html', '/024/lenesx-024-julie-klinger-rare-earth-frontiers-EN.html', '/008/lenesx-008-caro-RO.html' ],
        [ '/007/lenesx-007-nico-RO.html','/ls-003/lenesx-ls-003-iran-protests-EN.html','/009/lenesx-009-refugees-RO.html' ],
        [ '/008/lenesx-008-caro-RO.html','/027/lenesx-027-bab-balkan-anarchist-bookfair-2023-EN.html','/010/lenesx-010-electoralism-p1-RO.html' ],
        [ '/009/lenesx-009-refugees-RO.html','','/011/lenesx-011-electoralism-p2-RO.html' ],
        [ '/010/lenesx-010-electoralism-p1-RO.html','','/ls-001/lenesx-ls-001-interbrigadisti-RO.html' ],
        [ '/011/lenesx-011-electoralism-p2-RO.html','','/013/lenesx-013-she-ravelion-RO.html' ],
        [ '/012/lenesx-012-marina-EN.html','','/015/lenesx-015-caro-lumile-RO.html' ],
        [ '/ls-001/lenesx-ls-001-interbrigadisti-RO.html','','/018/lenesx-018-shera-trauma-abuz-RO.html' ],
        [ '/013/lenesx-013-she-ravelion-RO.html','','/019/lenesx-019-shera-madness-neurodiversitate-RO.html' ],
        [ '/014/lenesx-014-EAST-EN.html','','/020/lenesx-020-cosmin-dizabilitate-RO.html' ],
        [ '/015/lenesx-015-caro-lumile-RO.html', '', '/ls-002/lenesx-ls-002-dizabilitate-RO.html' ],
        [ '/016/lenesx-016-elmar-sergiu-migrant-EN.html', '', '/021/lenesx-021-ileana-poezie-queer-RO.html' ],           
        [ '/017/lenesx-017-rasha-free-palestine-EN.html', '', '/022/lenesx-022-marius-podzilnic-podcast-media-alternativ' ],
        [ '/018/lenesx-018-shera-trauma-abuz-RO.html', '', '/025/lenesx-025-nicu-locurile-memoriei-sclavia-si-holocaustul-impotriva-romilor-cu-Nicu-RO.html' ],
        [ '/019/lenesx-019-shera-madness-neurodiversitate-RO.html', '', '/026/lenesx-026-trifon-un-parcurs-libertar-cu-Nicolas-RO.html' ],
        [ '/020/lenesx-020-cosmin-dizabilitate-RO.html', '', '' ],
        [ '/ls-002/lenesx-ls-002-dizabilitate-RO.html', '', '' ],
        [ '/021/lenesx-021-ileana-poezie-queer-RO.html', '', '' ],
        [ '/022/lenesx-022-marius-podzilnic-podcast-media-alternativa-RO.html', '', '' ],
        [ '/023/lenesx-023-effrosyni-workers-self-management-viome-EN.html', '', '' ],
        [ '/024/lenesx-024-julie-klinger-rare-earth-frontiers-EN.html', '', '' ],
        [ '/025/lenesx-025-nicu-locurile-memoriei-sclavia-si-holocaustul-impotriva-romilor-cu-Nicu-RO.html', '', '' ],
        [ '/026/lenesx-026-trifon-un-parcurs-libertar-cu-Nicolas-RO.html', '', '' ],
        [ '/ls-003/lenesx-ls-003-iran-protests-EN.html', '', '' ],
        [ '/027/lenesx-027-bab-balkan-anarchist-bookfair-2023-EN.html', '', '' ]
    ];

    let $ep_no = Math.floor( Math.random() * $rand_no );
    
    window.location.replace( 'episodes' + $ep_list[$ep_no][$lan_no] );

}

function getRandom()
{
    let $lan_current = $(".lan_current").text();

    var $rand_list = [
        'Take me to a <a id="random_ep" onclick="getRandomEp()">random</a> episode.',               // all
        'Take me to a <a id="random_ep" onclick="getRandomEp()">random</a> episode.',               // en
        'Du-mă la un episod <a id="random_ep" onclick="getRandomEp()">la întâmplare.</a>',          // ro
        "WIP", "WIP",                                                                        // de
        "WIP", "WIP"                                                                         // hu
    ];

    var $lan_no = 0;
    if ( $lan_current == "All" ) {
        $lan_no = 0;
    } else if ( $lan_current == "EN" )  {
        $lan_no = 1;
    } else if ( $lan_current == "RO" )  {
        $lan_no = 2;
    } else if ( $lan_current == "DE" )  {
        $lan_no = 3;
    } else if ( $lan_current == "HU" )  {
        $lan_no = 4;
    }

    $(".random_ep").html( $rand_list[$lan_no] );
}
