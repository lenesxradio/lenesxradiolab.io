function getMotto()
{
    let $motto_no = Math.floor( Math.random() * 3 );

    var $motto_list =
        [ "Biased Towards the Voiceless",
          "Not Your Granddad's Lefty Magazine",
          "Like the New York Times Only Better" ];

    $(".logo_motto").html( $motto_list[$motto_no] );
}

$( document ).ready(function(){
    getMotto();
});
